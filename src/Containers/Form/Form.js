import React from 'react';
import Cards from '../Cards/Cards';
import { PageHeader } from 'react-bootstrap';

/**
 * 
 * @param {*} props 
 * : cards
 * : key
 */
const Form = (props) => {
    const cardArr = [];
    if(!!props.cards && !!props.cards.cards){
        props.cards.cards.map(card => {
            cardArr.push(<Cards cards={card} key={card.id}/>);
        });
    }
    return (
        <fieldset>
        <PageHeader>{props.cards.name}</PageHeader>
        
        {cardArr}
        </fieldset>
    )
}

export default Form;