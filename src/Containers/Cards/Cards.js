import React, {Component} from 'react';
import Card from '../../Components/Card/Card';
import { Panel, PanelGroup } from 'react-bootstrap';


class Cards extends Component{
    /**
     * @param {*} props 
     * 1. cards
     * 2. key
     */
    constructor(props){
        super();
    }

    render(){
        const resp = [];
        if(!!this.props.cards && !!this.props.cards.cards){
            this.props.cards.cards.map(card => { 
                resp.push(<Panel eventKey={this.props.cards.id}><Card content={card} key={card.id}/></Panel>);    
            })
            
        } else if(!!this.props.cards) {
            
                // resp.push(<Card content={this.props.cards} key={this.props.cards.id}/>)
            
        }
        
        return (
            <PanelGroup accordion id="accordion-example">
                {resp}
            </PanelGroup>
            );
        
    }
}

export default Cards;