import React, { Component } from 'react';
import Form from './Containers/Form/Form';
import './App.css';
var formResp = require('./MockFormResp.js').formResp;

class App extends Component {
  render() {
    return (
      <div className="App">
        {JSON.stringify(formResp)}
        <Form cards={formResp} key={formResp.id} />
        
      </div>
    );
  }
}

export default App;
