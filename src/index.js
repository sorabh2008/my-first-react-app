import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyAnImKb9XJnRoji86U1-PEq2JbqQOey0Rk",
    authDomain: "concrete-fusion-138323.firebaseapp.com",
    databaseURL: "https://concrete-fusion-138323.firebaseio.com",
    projectId: "concrete-fusion-138323",
    storageBucket: "concrete-fusion-138323.appspot.com",
    messagingSenderId: "389632279231"
  };

firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
