import React, { Component } from 'react';
import './input.css';
import { FormGroup, Col, ControlLabel, FormControl } from 'react-bootstrap';
/**
 * 
 * @param {*} props 
 * 1. field
 */
const Input = (props) => {
    // console.log('props --> ', props);
    return (
        <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
                {props.field.options.label}
            </Col>
            <Col sm={10}>
                <FormControl
                    id={props.field.id}
                    type={props.field[props.field.type].type}
                    placeholder={props.field[props.field.type].placeholder}
                />
            </Col>
        </FormGroup>


    )
}

export default Input;