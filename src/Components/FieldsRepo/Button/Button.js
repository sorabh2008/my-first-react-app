import React, { Component } from 'react';
import { Badge, Button } from 'react-bootstrap';

/**
 * 
 * @param {*} props 
 * 1. field
 */
class MyButton extends Component {
    constructor(props) {
        super();
    }

componentWillMount(){
    console.log("button-> component will mount");
}

componentDidMount(){
    console.log("button-> component did mount");
}

componentWillReceiveProps(newProps){
    console.log("button-> component will receive props", newProps);
}

shouldComponentUpdate(newProps, newState){
    console.log("button-> should component update", newProps, newState);
    return newProps.doUpdate
}

componentWillUpdate(newProps, newState){
    console.log("button-> component will update", newProps, newState);
}

componentDidUpdate(){
    console.log("button-> component did update");
}

    onClickHandler(data){
        console.log('clicked!!!', this.props.field.options.action);
        this.props.click();
    }

    render() {
        return (
            <p>
                <Button bsStyle="primary" name={this.props.field.options.label} 
                    onClick={this.onClickHandler.bind(this, {data: 'Hello'})}>
                    {this.props.field.options.label}
                </Button>
                <Badge>{this.props.clickCount}</Badge>
            </p>
        )
    }
}

export default MyButton;