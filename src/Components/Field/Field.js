import React from 'react';
import Input from '../FieldsRepo/input/input';
import MyButton from '../FieldsRepo/Button/Button';
import './Field.css';

/**
 * 
 * @param {*} props 
 * 1. field
 */
const Field = (props) => {
    let resp = [];
    console.log('switch on ->', props.field.type);
    switch(props.field.type){
        case 'input':
            resp.push(<Input field={props.field} key={props.field.id}/>);
            break;
        case 'button':
            resp.push(<MyButton field={props.field} key={props.field.id}/>);
            break;
        default:

    }
 return  resp;
 
}

export default Field;