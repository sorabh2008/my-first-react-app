export const formResp = {
    id: '123',
    name: 'Demo Form',
    cards: [
      {
        id: '1',
        name: 'Sign in / up',
        cards: [
          {
            id: '21',
            name: 'Sign in',
            fields: [
              {
                id: '1112',
                type: 'input',
                options: {
                  label: 'User ID'
                },
                input: {
                  type: 'text',
                  value: 'ABC',
                  placeholder: 'enter name',
                  defaultRef: '102'
                },
                required: true,
                validation: {
                  rule: 'not-empty'
                }
              }, {
                id: '1113',
                type: 'input',
                input: {
                  type: 'password',
                  placeholder: 'enter password'
                },
                options: {
                  label: 'Password'
                },
                required: true
              }
            ],
            navigation: [
              {
                id: '1191',
                type: 'button',
                options: {
                  action: 'login',
                  agrs: [
                    {
                      name:'user',
                      fieldRef: "102"
                    },
                    {
                      name:'pass',
                      fieldRef: '103'
                    }
                  ],
                  label: 'Login'
                }
              }
            ]
          },
          {
            id: '1001',
            name: 'Sign up',
            fields: [
              {
                id: '104',
                type: 'input',
                input: {
                  type: 'text',
                  placeholder: 'user@example.com'
                },
                options: {
                  label: 'Email ID'
                },
                required: true,
                validation: {
                  rule: 'email'
                }
              },
              {
                id: '102',
                type: 'input',
                options: {
                  label: 'User ID'
                },
                input: {
                  type: 'text',
                  value: 'ABC',
                  placeholder: 'enter name'
                },
                required: true,
                validation: {
                  rule: 'not-empty'
                }
              }, {
                id: '103',
                type: 'input',
                input: {
                  type: 'password',
                  placeholder: 'enter password'
                },
                options: {
                  label: 'Password'
                },
                required: true
              }
            ],
            navigation: [
              {
                id: '105',
                type: 'button',
                options: {
                  label: 'Register'
                }
              }
            ]
          }
        ]
      }
    ]
  };
  